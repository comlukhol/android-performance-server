package com.lukhol.mathesis.android.performance.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public interface FixupService {
    void deleteChildrenOfStarupTest(List<String> brokenSummaryIds);
    void deleteWrongSummaries(List<String> brokenSummaryIds);
    void deleteTestsByTestSummaryIds(List<String> testSummaryIds);
    void deleteStartupTestsBySummaryIds(List<String> allBrokenSummaryIds);

    @Transactional
    void deleteStartupTestsWhereTestSummaryIsEmpty();
}

//http://ignaciosuay.com/why-is-spring-ignoring-transactional/