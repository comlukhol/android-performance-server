package com.lukhol.mathesis.android.performance.validator;

import com.lukhol.mathesis.android.performance.dto.TestDto;
import com.lukhol.mathesis.android.performance.exception.ServiceValidationException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TestValidator implements Validator {

    private Errors errors;

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass == TestDto.class;
    }

    public void validate(Object obj, Errors errors) throws ServiceValidationException {
        this.errors = errors;
        TestDto testDto = (TestDto)obj;
        rejectIfTrue(testDto.getTestCode() == null || testDto.getTestId().equals(""), "testId", "validation.notEmpty");
        rejectIfTrue(testDto.getApplicationType() == null, "applicationCode", "validation.notEmpty");
        rejectIfTrue(testDto.getTestCode() == null, "testCode", "validation.notEmpty");

        if(this.errors.hasErrors())
            throw new ServiceValidationException(this.errors);
    }

    private void rejectIfFalse(boolean condition, String fieldName, String messageCode) {
        if(!condition)
            this.errors.rejectValue(fieldName, messageCode);
    }

    private void rejectIfTrue(boolean condition, String fieldName, String messageCode) {
        if(condition)
            this.errors.rejectValue(fieldName, messageCode);
    }
}