package com.lukhol.mathesis.android.performance.repository;

import com.lukhol.mathesis.android.performance.model.Test;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TestRepository extends JpaRepository<Test, Long>, TestRepositoryCustom {

    @Query("select distinct t from Test t where t.testSummary.id = :testSummaryId")
    List<Test> findAllTestsByTestId(@Param("testSummaryId") String testSummaryId);

    @Query("select distinct t.testSummary.id from Test t")
    List<String> findAllTestSummaryIdsPresentedInTests();

    @Modifying
    @Query("delete from Test t where t.testSummary.id in (:testSummaryIds)")
    void removeByTestSummaryIds(@Param("testSummaryIds") List<String> testSummaryIds);
}