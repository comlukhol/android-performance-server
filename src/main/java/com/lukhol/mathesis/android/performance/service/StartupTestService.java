package com.lukhol.mathesis.android.performance.service;

import com.lukhol.mathesis.android.performance.dto.StartupTestDto;

import java.util.List;

public interface StartupTestService {
    void save(StartupTestDto startupTestDto);
    StartupTestDto findStartupResultsByTestId(String testId);
    List<StartupTestDto> findAllStartupResults();
}