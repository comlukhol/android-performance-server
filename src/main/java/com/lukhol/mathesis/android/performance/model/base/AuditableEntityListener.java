package com.lukhol.mathesis.android.performance.model.base;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.util.Date;

public class AuditableEntityListener {

    @PreUpdate
    public void onUpdate(Auditable auditable) {
        Date date = new Date();
        auditable.setLastModifiedDate(date);
    }

    @PrePersist
    public void onPersist(Auditable auditable) {
        Date date = new Date();
        auditable.setCreationDate(date);
    }
}