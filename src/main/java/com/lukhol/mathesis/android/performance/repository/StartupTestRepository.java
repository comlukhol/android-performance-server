package com.lukhol.mathesis.android.performance.repository;

import com.lukhol.mathesis.android.performance.model.StartupTest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StartupTestRepository extends JpaRepository<StartupTest, Long> {

    @Query("select st from StartupTest st where st.testSummary.id = :testSummaryId")
    Optional<StartupTest> findByTestSummaryId(@Param("testSummaryId") String testSummaryId);

    @Query("select distinct sm from StartupTest sm")
    List<StartupTest> findAll();

    @Modifying
    @Query("delete from StartupTest st where st.testSummary.id = :testSummaryId")
    void deleteBySummaryId(@Param("testSummaryId") String testSummaryId);

    @Modifying
    @Query(value = "delete from startup_test_startup_results where startup_test_id IN (SELECT id from startup_test where test_summary_id IN (:testSummaryIds))", nativeQuery = true)
    void deleteChildrenOfStartupTestEmbeddedBug(@Param("testSummaryIds") List<String> testSummaryIds);

    @Modifying
    @Query("delete from StartupTest st where st.testSummary.id in (:testSummaryIds)")
    void deleteBySummaryIds(@Param("testSummaryIds") List<String> testSummaryIds);

    @Modifying
    @Query("delete from StartupTest st where st.testSummary = null")
    void deleteWhereTestSummaryIsNull();
}