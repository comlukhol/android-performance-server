package com.lukhol.mathesis.android.performance.repository;

import com.lukhol.mathesis.android.performance.model.Test;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class TestRepositoryImpl implements TestRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Test> getFirstTen() {
        List<Test> tests = entityManager.createQuery("select t from Test t where t.id <= 10").getResultList();

        entityManager.flush();
        entityManager.clear();

        return tests;
    }

    @Override
    public List<Test> getSecondPart() {
        List<Test> tests = entityManager.createQuery("select t from Test t where t.id > 10").getResultList();

        entityManager.flush();
        entityManager.clear();

        return tests;
    }
}
