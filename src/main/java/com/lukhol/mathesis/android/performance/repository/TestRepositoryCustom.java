package com.lukhol.mathesis.android.performance.repository;

import com.lukhol.mathesis.android.performance.model.Test;

import java.util.List;

public interface TestRepositoryCustom {
    List<Test> getFirstTen();
    List<Test> getSecondPart();
}