package com.lukhol.mathesis.android.performance.service;

import com.lukhol.mathesis.android.performance.dto.TestDto;
import com.lukhol.mathesis.android.performance.dto.TestSummaryDto;
import com.lukhol.mathesis.android.performance.dto.TestSummaryMetadata;
import com.lukhol.mathesis.android.performance.model.Test;
import com.lukhol.mathesis.android.performance.model.TestSummary;

import java.util.List;

public interface TestService {
    void save(TestDto testDto);
    TestSummaryDto getSummary(String testId);
    List<String> getAllTestSummaryIds();
    List<TestSummaryDto> getAllSummaries();
    TestSummaryDto fromTestSummaryToDto(TestSummary testSummary);

    TestSummary getAverageForSummaryIds(List<String> summaryIds);
    TestSummary getAverageForAndroidVersion(String androidVersion);

    List<String> getAllAndroidVersions();

    List<TestSummaryMetadata> getAllTestSummariesMetadata();

    List<Test> getSomeTest();

    void saveExtraStartupTime(String summaryId, long elapsedMillis);
}