package com.lukhol.mathesis.android.performance.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TestSummaryDto {

    private String testId;
    private String date;
    private StartupTestDto startupTest;
    private List<TestDto> tests;
    private String androidVersion;
}