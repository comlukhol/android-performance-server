package com.lukhol.mathesis.android.performance.service;

import com.lukhol.mathesis.android.performance.repository.StartupTestRepository;
import com.lukhol.mathesis.android.performance.repository.TestRepository;
import com.lukhol.mathesis.android.performance.repository.TestSummaryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class FixupServiceImpl implements FixupService{

    private final TestSummaryRepository testSummaryRepository;
    private final StartupTestRepository startupTestRepository;
    private final TestRepository testRepository;

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void deleteChildrenOfStarupTest(List<String> brokenSummaryIds) {
        if(!brokenSummaryIds.isEmpty())
            startupTestRepository.deleteChildrenOfStartupTestEmbeddedBug(brokenSummaryIds);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void deleteWrongSummaries(List<String> brokenSummaryIds) {
        if(!brokenSummaryIds.isEmpty())
            testSummaryRepository.deleteByIds(brokenSummaryIds);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void deleteTestsByTestSummaryIds(List<String> testSummaryIds) {
        if(!testSummaryIds.isEmpty())
            testRepository.removeByTestSummaryIds(testSummaryIds);
    }

    @Override
    @Transactional
    public void deleteStartupTestsBySummaryIds(List<String> allBrokenSummaryIds) {
        if(!allBrokenSummaryIds.isEmpty())
            startupTestRepository.deleteBySummaryIds(allBrokenSummaryIds);
    }

    @Override
    @Transactional
    public void deleteStartupTestsWhereTestSummaryIsEmpty() {
        startupTestRepository.deleteWhereTestSummaryIsNull();
    }
}