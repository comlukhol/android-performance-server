package com.lukhol.mathesis.android.performance.model;

public enum ApplicationType {
    NATIVE,
    CORDOVA,
    XAMARIN_FORMS
}