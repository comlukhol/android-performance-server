package com.lukhol.mathesis.android.performance.controller;

import com.lukhol.mathesis.android.performance.dto.StartupTestDto;
import com.lukhol.mathesis.android.performance.dto.TestDto;
import com.lukhol.mathesis.android.performance.repository.StartupTestRepository;
import com.lukhol.mathesis.android.performance.repository.TestRepository;
import com.lukhol.mathesis.android.performance.service.StartupTestService;
import com.lukhol.mathesis.android.performance.service.TestService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api/v1/test/")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TestController {

    private final StartupTestService startupTestService;
    private final TestService testService;

    @PostMapping("/startup")
    public void home(@RequestBody StartupTestDto startupTestDto,
                     final HttpServletResponse response) {
        startupTestService.save(startupTestDto);
    }

    @PostMapping("/save")
    public void saveTest(@RequestBody TestDto testDto) {
        testService.save(testDto);
    }

    @PostMapping("/extraStartTime/save/{summaryId}/{elapsedMillis}")
    public void saveExtraStartTime(@PathVariable("summaryId") String summaryId,
                                   @PathVariable("elapsedMillis") double elapsedMillis) {
        testService.saveExtraStartupTime(summaryId, Math.round(elapsedMillis));
    }
}