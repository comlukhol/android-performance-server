package com.lukhol.mathesis.android.performance.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.Expose;
import com.lukhol.mathesis.android.performance.dto.StartupResultDto;
import com.lukhol.mathesis.android.performance.model.base.Auditable;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class StartupTest extends Auditable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "myNative")
    protected Long id;

    @JsonIgnore
    @ManyToOne
    protected TestSummary testSummary;

    @Embedded
    @ElementCollection
    @CollectionTable//(foreignKey = @ForeignKey(value = ConstraintMode.NO_CONSTRAINT)) //Default is CONSTRAINT
    protected List<StartupResultDto> startupResults;
}