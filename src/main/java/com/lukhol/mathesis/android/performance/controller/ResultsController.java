package com.lukhol.mathesis.android.performance.controller;

import com.lukhol.mathesis.android.performance.dto.StartupTestDto;
import com.lukhol.mathesis.android.performance.model.TestSummary;
import com.lukhol.mathesis.android.performance.service.StartupTestService;
import com.lukhol.mathesis.android.performance.service.TestService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/results/")
@CrossOrigin(origins = {"http://localhost:3000", "http://70eebe13.ngrok.io", "https://70eebe13.ngrok.io"})
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ResultsController {

    private final StartupTestService startupTestService;
    private final TestService testService;

    @GetMapping("/ids")
    public ResponseEntity<?> getAllTestSummaryIds() {
        return new ResponseEntity<>(testService.getAllTestSummaryIds(), HttpStatus.OK);
    }

    @GetMapping("/summaryMetadata")
    public ResponseEntity<?> getAllTestSummariesMetadata() {
        return new ResponseEntity<>(testService.getAllTestSummariesMetadata(), HttpStatus.OK);
    }

    @GetMapping("/androidVersions")
    public ResponseEntity<?> getAllAndroidVersions() {
        return new ResponseEntity<>(testService.getAllAndroidVersions(), HttpStatus.OK);
    }

    @GetMapping("/startup/{testId}")
    public ResponseEntity<StartupTestDto> startupTestResults(@PathVariable String testId) {
        return new ResponseEntity<>(startupTestService.findStartupResultsByTestId(testId), HttpStatus.OK);
    }

    @GetMapping("/startup")
    public ResponseEntity<List<StartupTestDto>> allStartupTestResults() {
        return new ResponseEntity<>(startupTestService.findAllStartupResults(), HttpStatus.OK);
    }

    @GetMapping("/summary/{testId}")
    public ResponseEntity<?> testSummary(@PathVariable String testId) {
        return new ResponseEntity<>(testService.getSummary(testId), HttpStatus.OK);
    }

    @GetMapping("/summary")
    public ResponseEntity<?> allTestSummary() {
        return new ResponseEntity<>(testService.getAllSummaries(), HttpStatus.OK);
    }

    @GetMapping("/summary/average/ids")
    public ResponseEntity<?> getAverageForIds(@RequestParam("testSummaryIds") List<String> testSummaryIds) {
        TestSummary averageTestSummary = testService.getAverageForSummaryIds(testSummaryIds);
        return new ResponseEntity<>(testService.fromTestSummaryToDto(averageTestSummary), HttpStatus.OK);
    }

    @GetMapping("/summary/average/androidVersion")
    public ResponseEntity<?> getAverageForAndroidVersion(@RequestParam("version") String androidVersion) {
        TestSummary averageTestSummary = testService.getAverageForAndroidVersion(androidVersion);
        return new ResponseEntity<>(testService.fromTestSummaryToDto(averageTestSummary), HttpStatus.OK);
    }
}