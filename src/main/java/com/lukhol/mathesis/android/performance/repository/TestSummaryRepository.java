package com.lukhol.mathesis.android.performance.repository;

import com.lukhol.mathesis.android.performance.dto.TestSummaryMetadata;
import com.lukhol.mathesis.android.performance.model.TestSummary;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TestSummaryRepository extends CrudRepository<TestSummary, String> {

    @Query("select distinct ts from TestSummary ts left join fetch ts.startupTest st left join fetch st.startupResults where ts.id in (:summaryIds)")
    List<TestSummary> findAllByIds(@Param("summaryIds") List<String> summaryIds);

    @Query("select distinct ts.id from TestSummary ts")
    List<String> findAllIds();

    @Modifying
    @Query("delete from TestSummary ts where ts.id in (:ids)")
    void deleteByIds(@Param("ids") List<String> ids);

    @Query("select ts.id from TestSummary ts where ts.androidVersion = :androidVersion")
    List<String> findAllIdsByAndroidVersion(@Param("androidVersion") String androidVersion);

    @Query("select distinct ts.androidVersion from TestSummary ts where ts.androidVersion is not null order by ts.androidVersion")
    List<String> findAllAndroidVersions();

    @Query("select new com.lukhol.mathesis.android.performance.dto.TestSummaryMetadata(ts.id, ts.androidVersion, '') from TestSummary ts")
    List<TestSummaryMetadata> findAllTestSummaryMetadata();
}