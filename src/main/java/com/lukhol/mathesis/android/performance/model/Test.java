package com.lukhol.mathesis.android.performance.model;

import lombok.*;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter

public class Test {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "myNative")
    protected Long id;

    @Enumerated(EnumType.STRING)
    protected TestCode testCode;

    @Enumerated(EnumType.STRING)
    protected ApplicationType applicationType;

    @Column(nullable = false)
    protected long time;

    @ManyToOne
    protected TestSummary testSummary;
}
