package com.lukhol.mathesis.android.performance;

import com.lukhol.mathesis.android.performance.dto.StartupTestDto;
import com.lukhol.mathesis.android.performance.model.StartupTest;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperConfig {

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();

        modelMapper.createTypeMap(StartupTestDto.class, StartupTest.class)
                .addMappings(m -> m.skip(StartupTest::setId));

        return modelMapper;
    }
}