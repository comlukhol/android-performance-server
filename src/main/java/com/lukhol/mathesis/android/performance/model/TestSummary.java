package com.lukhol.mathesis.android.performance.model;

import com.lukhol.mathesis.android.performance.model.base.Auditable;
import lombok.*;
import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TestSummary extends Auditable {

    @Id
    protected String id;

    protected String androidVersion;

    @OneToOne(mappedBy = "testSummary", orphanRemoval = true, cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    protected StartupTest startupTest;

    @BatchSize(size = 20)
    @OneToMany(mappedBy = "testSummary", fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.REMOVE)
    protected List<Test> tests = new ArrayList<>();

    protected long extraStartTime;
}
