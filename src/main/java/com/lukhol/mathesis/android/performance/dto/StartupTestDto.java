package com.lukhol.mathesis.android.performance.dto;

import com.lukhol.mathesis.android.performance.model.TestCode;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class StartupTestDto {
    private String summaryId;
    private String androidVersion;
    private TestCode testCode;
    private List<StartupResultDto> results;
}