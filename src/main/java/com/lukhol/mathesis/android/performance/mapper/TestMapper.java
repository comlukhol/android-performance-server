package com.lukhol.mathesis.android.performance.mapper;

import com.lukhol.mathesis.android.performance.dto.TestDto;
import com.lukhol.mathesis.android.performance.model.Test;
import com.lukhol.mathesis.android.performance.model.TestSummary;
import com.lukhol.mathesis.android.performance.repository.TestSummaryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TestMapper {
    public TestDto toDto(Test test) {
        return TestDto.builder()
                .applicationType(test.getApplicationType())
                .testCode(test.getTestCode())
                .testId(test.getTestSummary() != null ? test.getTestSummary().getId() : null)
                .time(test.getTime())
                .build();
    }

    public Test fromDto(TestDto testDto) {

        return Test.builder()
                .testCode(testDto.getTestCode())
                .applicationType(testDto.getApplicationType())
                .time(testDto.getTime())
                .build();
    }
}