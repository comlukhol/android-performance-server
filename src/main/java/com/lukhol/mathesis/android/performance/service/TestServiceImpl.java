package com.lukhol.mathesis.android.performance.service;

import com.lukhol.mathesis.android.performance.dto.*;
import com.lukhol.mathesis.android.performance.mapper.TestMapper;
import com.lukhol.mathesis.android.performance.model.*;
import com.lukhol.mathesis.android.performance.repository.StartupTestRepository;
import com.lukhol.mathesis.android.performance.repository.TestRepository;
import com.lukhol.mathesis.android.performance.repository.TestSummaryRepository;
import com.lukhol.mathesis.android.performance.validator.TestValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BeanPropertyBindingResult;

import java.util.Date;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TestServiceImpl implements TestService {

    private final StartupTestRepository startupTestRepository;
    private final TestRepository testRepository;
    private final TestSummaryRepository testSummaryRepository;
    private final FixupService fixupService;
    private final TestMapper testMapper;
    private final TestValidator testValidator;

    @Override
    @Transactional
    public void save(TestDto testDto) {
        testValidator.validate(testDto, new BeanPropertyBindingResult(testDto, "testDto"));
        Test test = testMapper.fromDto(testDto);

        TestSummary testSummary = testSummaryRepository.findById(testDto.getTestId()).orElse(null);
        if(testSummary == null) {
            testSummary = new TestSummary();
            testSummary.setId(testDto.getTestId());
            testSummaryRepository.save(testSummary);
        }

        boolean isUnique = testSummary.getTests()
                .stream()
                .filter(item -> item.getTestCode() == test.getTestCode() && item.getApplicationType() == test.getApplicationType())
                .collect(Collectors.toList())
                .isEmpty();

        if(isUnique) {
            testSummary.getTests().add(test);
            test.setTestSummary(testSummary);
            testRepository.save(test);
        }
    }

    @Override
    public List<String> getAllTestSummaryIds() {
        return testSummaryRepository.findAllIds();
    }

    @Override
    @Transactional
    public TestSummaryDto getSummary(String testId) {
        TestSummary testSummary = testSummaryRepository.findById(testId).orElseThrow(NoSuchElementException::new);
        TestSummaryDto testSummaryDto = fromTestSummaryToDto(testSummary);

        if(testSummary.getTests().size()%21 != 0 || testSummary.getAndroidVersion() == null)
            return null;

        return testSummaryDto;
    }

    @Override
    public List<TestSummaryDto> getAllSummaries() {
        List<String> allTestsIds = getAllTestSummaryIds();
        if(allTestsIds.isEmpty())
            return new ArrayList<>();

        List<TestSummary> allSummaries = testSummaryRepository.findAllByIds(allTestsIds);

        return allSummaries.stream()
                .filter(this::checkSummaryCorrectness)
                .map(this::fromTestSummaryToDto)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Override
    public TestSummary getAverageForSummaryIds(List<String> summaryIds) {
        List<TestSummary> testSummaries = testSummaryRepository.findAllByIds(summaryIds).stream()
                .filter(this::checkSummaryCorrectness)
                .collect(Collectors.toList());

        return createAverageSummaryForSummaries(testSummaries);
    }

    @Override
    public TestSummary getAverageForAndroidVersion(String androidVersion) {
        return getAverageForSummaryIds(testSummaryRepository.findAllIdsByAndroidVersion(androidVersion));
    }

    @Override
    public List<String> getAllAndroidVersions() {
        return testSummaryRepository.findAllAndroidVersions();
    }

    @Override
    public List<TestSummaryMetadata> getAllTestSummariesMetadata() {
        return testSummaryRepository.findAllTestSummaryMetadata();
    }

    @Override
    @Transactional
    public List<Test> getSomeTest() {
        List<Test> t1 = testRepository.getFirstTen();
        List<Test> t2 = testRepository.getSecondPart();

        return new ArrayList<>();
    }

    @Override
    @Transactional
    public void saveExtraStartupTime(String summaryId, long elapsedMillis) {
        Optional<TestSummary> testSummaryOptional = testSummaryRepository.findById(summaryId);
        if(testSummaryOptional.isPresent()) {
            TestSummary testSummary = testSummaryOptional.get();
            testSummary.setExtraStartTime(elapsedMillis);
        }
    }

    @Override
    public TestSummaryDto fromTestSummaryToDto(TestSummary testSummary) {
        List<TestDto> testDtos = testSummary.getTests()
                .stream()
                .map(testMapper::toDto)
                .collect(Collectors.toList());

        StartupTest startupResultDto = testSummary.getStartupTest();
        for(StartupResultDto srdto : startupResultDto.getStartupResults()){
            if(srdto.getApplicationType().equals(ApplicationType.CORDOVA)) {
                long startupTime = testSummary.getExtraStartTime() + srdto.getThisTime();
                srdto.setThisTime(startupTime);
            }
        }

        return TestSummaryDto.builder()
                .startupTest(startupTestToDto(startupResultDto))
                .testId(testSummary.getId())
                .date(testSummary.getCreationDate().toString())
                .androidVersion(testSummary.getAndroidVersion())
                .tests(testDtos)
                .build();
    }

    private StartupTestDto startupTestToDto(StartupTest startupTest) {
        return StartupTestDto.builder()
                .testCode(TestCode.START_UP)
                .results(startupTest.getStartupResults())
                .build();
    }

    private TestSummary createAverageSummaryForSummaries(List<TestSummary> testSummaries) {
        TestSummary firstTestSummary = testSummaries.get(0);

        String averageSummaryId = createAverageSummaryId(testSummaries);
        String androidVersion = createAndroidVersionAverageString(testSummaries);
        List<Test> averageTestList = createStandardTestsAverage(testSummaries);
        StartupTest startupTest = createStartupTestsAverage(testSummaries);

        TestSummary testSummary = TestSummary.builder()
                .id(averageSummaryId)
                .tests(averageTestList)
                .androidVersion(androidVersion)
                .startupTest(startupTest)
                .build();

        testSummary.setCreationDate(new Date());
        return testSummary;
    }

    private String createAverageSummaryId(List<TestSummary> testSummaries) {
        if(testSummaries.isEmpty())
            return null;

        Set<String> testSummaryIds = testSummaries.stream()
                .map(TestSummary::getId)
                .collect(Collectors.toSet());

        return String.join(", ", testSummaryIds);
    }

    private String createAndroidVersionAverageString(List<TestSummary> testSummaries) {
        if(testSummaries.isEmpty())
            return null;

        Set<String> androidVersions = testSummaries.stream()
                .map(TestSummary::getAndroidVersion)
                .collect(Collectors.toSet());

        return String.join(", ", androidVersions);
    }

    private StartupTest createStartupTestsAverage(List<TestSummary> testSummaries) {
        List<StartupTest> startupTests = testSummaries.stream()
                .map(TestSummary::getStartupTest)
                .collect(Collectors.toList());

        List<StartupResultDto> startupResultDtos = new ArrayList<>();
        for(ApplicationType appType : ApplicationType.values()) {
            startupResultDtos.add(createAverageForAppType(startupTests, appType));
        }

        return StartupTest.builder()
                .startupResults(startupResultDtos)
                .build();
    }

    private StartupResultDto createAverageForAppType(List<StartupTest> startupTests, ApplicationType appType) {
        long thisTimeAverage = 0, waitTimeAverage = 0, totalTimeAverage = 0;
        int count = 0;
        for(StartupTest startupTest : startupTests) {
            List<StartupResultDto> startupResultDtos = startupTest.getStartupResults();
            for(StartupResultDto startupResultDto : startupResultDtos) {
                if(startupResultDto.getApplicationType().equals(appType)) {
                    thisTimeAverage += startupResultDto.getThisTime();
                    waitTimeAverage += startupResultDto.getWaitTime();
                    totalTimeAverage += startupResultDto.getTotalTime();
                    count++;
                }
            }
        }

        return StartupResultDto.builder()
                .applicationType(appType)
                .thisTime(thisTimeAverage/count)
                .waitTime(waitTimeAverage/count)
                .totalTime(totalTimeAverage/count)
                .build();
    }

    private List<Test> createStandardTestsAverage(List<TestSummary> testSummaries) {
        List<Test> averageTestList = new ArrayList<>();

        for(ApplicationType appType : ApplicationType.values()) {
            for(TestCode testCode : TestCode.values()) {
                List<Test> matchingTests = findAllTestsByTestCodeAndAppType(testSummaries, testCode, appType);
                Test averageTest = createAverageForTests(matchingTests);
                averageTestList.add(averageTest);
            }
        }

        averageTestList.removeIf(Objects::isNull);

        return averageTestList;
    }

    private Test createAverageForTests(List<Test> matchingTests) {
        if(matchingTests.isEmpty())
            return null;

        double averageTime = matchingTests.stream()
                .mapToLong(Test::getTime)
                .average()
                .getAsDouble();

        return Test.builder()
                .testCode(matchingTests.get(0).getTestCode())
                .applicationType(matchingTests.get(0).getApplicationType())
                .time((long)averageTime)
                .build();
    }

    private List<Test> findAllTestsByTestCodeAndAppType(List<TestSummary> testSummaries, TestCode testCode, ApplicationType appType) {
        List<Test> allTests = new ArrayList<>();
        for(TestSummary testSummary : testSummaries) {
            allTests.addAll(testSummary.getTests());
        }

        return allTests.stream()
                .filter(test -> testCode.equals(test.getTestCode()) && appType.equals(test.getApplicationType()))
                .collect(Collectors.toList());
    }

    private boolean checkSummaryCorrectness(TestSummary testSummary) {

        List<ApplicationType> applicationTypes = testSummary.getTests().stream()
                .map(Test::getApplicationType)
                .collect(Collectors.toList());

        if(!applicationTypes.containsAll(Arrays.asList(ApplicationType.values()))) {
            return false;
        }

        if(testSummary.getStartupTest() == null || testSummary.getStartupTest().getStartupResults().size() != 3)
            return false;

        if(testSummary.getAndroidVersion() == null || "".equals(testSummary.getAndroidVersion()))
            return false;

//        if(testSummary.getTests().size() != 27)
//            return false;

        return true;
    }

    private boolean checkSummaryCorrectnessAfterTime(TestSummary testSummary) {
        if(new Date().getTime() - testSummary.getCreationDate().getTime() < 6000000)
            return false;

        if(testSummary.getTests().size()%27 != 0)
            return false;

        if(testSummary.getAndroidVersion() == null)
            return false;

        return true;
    }

    //@Transactional
//    @Scheduled(fixedDelay = 60000000)
//    public void removeWrongTests() {
//        List<String> allTestsIds = getAllTestSummaryIds();
//
//        if(allTestsIds.isEmpty())
//            return;
//
//        List<String> allBrokenSummaryIds = testSummaryRepository
//                .findAllByIds(allTestsIds)
//                .stream()
//                .filter(t -> !checkSummaryCorrectnessAfterTime(t))
//                .filter(t -> !checkSummaryCorrectness(t))
//                .map(TestSummary::getId)
//                .collect(Collectors.toList());
//
//        if(!allBrokenSummaryIds.isEmpty()) {
//            fixupService.deleteChildrenOfStarupTest(allBrokenSummaryIds);
//            fixupService.deleteStartupTestsBySummaryIds(allBrokenSummaryIds);
//            fixupService.deleteTestsByTestSummaryIds(allBrokenSummaryIds);
//            fixupService.deleteWrongSummaries(allBrokenSummaryIds);
//        }
//
//        fixupService.deleteStartupTestsWhereTestSummaryIsEmpty();
//    }
}