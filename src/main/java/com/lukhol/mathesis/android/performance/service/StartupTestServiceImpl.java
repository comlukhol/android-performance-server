package com.lukhol.mathesis.android.performance.service;

import com.lukhol.mathesis.android.performance.dto.StartupTestDto;
import com.lukhol.mathesis.android.performance.model.StartupTest;
import com.lukhol.mathesis.android.performance.model.TestCode;
import com.lukhol.mathesis.android.performance.model.TestSummary;
import com.lukhol.mathesis.android.performance.repository.StartupTestRepository;
import com.lukhol.mathesis.android.performance.repository.TestSummaryRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class StartupTestServiceImpl implements StartupTestService {

    private final StartupTestRepository startupTestRepository;
    private final TestSummaryRepository testSummaryRepository;
    private final ModelMapper modelMapper;

    @Override
    public void save(StartupTestDto startupTestDto) {
        StartupTest startupTest = modelMapper.map(startupTestDto, StartupTest.class);
        startupTestRepository.save(startupTest);

        TestSummary testSummary = testSummaryRepository.findById(startupTestDto.getSummaryId()).orElse(null);
        if(testSummary == null) {
            testSummary = new TestSummary();
            testSummary.setId(startupTestDto.getSummaryId());
            testSummaryRepository.save(testSummary);
        }

        testSummary.setStartupTest(startupTest);
        testSummary.setAndroidVersion(startupTestDto.getAndroidVersion());
        startupTest.setTestSummary(testSummary);
    }

    @Override
    public StartupTestDto findStartupResultsByTestId(String testId) {
        Optional<StartupTest> startupTestOptional = startupTestRepository.findByTestSummaryId(testId);
        StartupTest st = startupTestOptional.orElseThrow(NoSuchElementException::new);
        return toDto(st);
    }

    @Override
    public List<StartupTestDto> findAllStartupResults() {
        return startupTestRepository
                .findAll()
                .stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    private StartupTest fromDto(StartupTestDto startupTestDto) {
        return StartupTest.builder()
                .startupResults(startupTestDto.getResults())
                .testSummary(testSummaryRepository.findById(startupTestDto.getSummaryId()).orElse(null))
                .build();
    }

    private StartupTestDto toDto(StartupTest startupTest) {
        TestSummary testSummary = startupTest.getTestSummary();

        return StartupTestDto.builder()
                .results(startupTest.getStartupResults())
                .androidVersion(testSummary != null ? testSummary.getAndroidVersion() : null)
                .testCode(TestCode.START_UP)
                .summaryId(testSummary != null ? testSummary.getId() : null)
                .build();
    }
}