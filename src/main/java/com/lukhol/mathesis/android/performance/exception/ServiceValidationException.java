package com.lukhol.mathesis.android.performance.exception;

import org.springframework.validation.Errors;

/*
 * Throw this exception with BindingResult implementation, eg. new BeanPropertyBindingResult(commentDTO, "comment"),
 * then rejectValue with (code, messageCode) and throw this exception with that BindingResult.
 */
public class ServiceValidationException extends RuntimeException {
    private static final long serialVersionUID = 7984245554303334160L;

    private final Errors errors;

    public ServiceValidationException(Errors errors) {
        this.errors = errors;
    }

    public Errors getErrors() {
        return errors;
    }
}