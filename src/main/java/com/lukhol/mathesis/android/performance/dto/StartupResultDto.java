package com.lukhol.mathesis.android.performance.dto;

import com.lukhol.mathesis.android.performance.model.ApplicationType;
import lombok.*;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
@Builder
@ToString
@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class StartupResultDto {

    @Enumerated(EnumType.STRING)
    private ApplicationType applicationType;
    private long thisTime;
    private long totalTime;
    private long waitTime;
    private long extraTime;
}