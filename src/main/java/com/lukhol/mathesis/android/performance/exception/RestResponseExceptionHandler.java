package com.lukhol.mathesis.android.performance.exception;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;

@ControllerAdvice
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RestResponseExceptionHandler {

    private final MessageSource messageSource;

    @ExceptionHandler(value = {NoSuchElementException.class})
    protected ResponseEntity<?> missingResourcesHandler(NoSuchElementException ex, WebRequest request) {
        ex.printStackTrace();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @ExceptionHandler(value = {ServiceValidationException.class})
    public ResponseEntity<?> handleServiceValidationException(ServiceValidationException error, WebRequest request) {
        String acceptLanguage = request.getHeader("Accept-Language");

        if(acceptLanguage == null || !acceptLanguage.contains("en") && !acceptLanguage.contains("pl"))
            acceptLanguage = "en";

        return parseErrors(error.getErrors(), acceptLanguage);
    }

    private ResponseEntity<Map<String, String>> parseErrors(Errors errors, String acceptLanguage) {
        Map<String, String> errorsMap = new HashMap<>();
        for(ObjectError objectError : errors.getAllErrors()) {
            FieldError fieldError = (FieldError)objectError;
            if(acceptLanguage != null) {
                try {
                    String message = null;

                    try {
                        message = messageSource.getMessage(fieldError.getCode(), null, Locale.forLanguageTag(acceptLanguage));
                    } catch (NoSuchMessageException e) {
                        message = fieldError.getDefaultMessage();

                        if(message == null)
                            message = "Validation error.";
                    }

                    errorsMap.put(fieldError.getField(), message);
                } catch (NoSuchMessageException e) {
                    errorsMap.put(fieldError.getField(), objectError.getDefaultMessage());
                }
            } else {
                errorsMap.put(fieldError.getField(), objectError.getDefaultMessage());
            }
        }

        return new ResponseEntity<>(errorsMap, HttpStatus.BAD_REQUEST);
    }
}