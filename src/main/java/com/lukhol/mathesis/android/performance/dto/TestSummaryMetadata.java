package com.lukhol.mathesis.android.performance.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TestSummaryMetadata {
    private String id;
    private String androidVersion;
    private String deviceId;
}