package com.lukhol.mathesis.android.performance.dto;

import com.lukhol.mathesis.android.performance.model.ApplicationType;
import com.lukhol.mathesis.android.performance.model.TestCode;
import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TestDto {
    private String testId;
    private TestCode testCode;
    private ApplicationType applicationType;
    private long time;
}