package com.lukhol.mathesis.android.performance.repository;

import com.lukhol.mathesis.android.performance.model.ApplicationType;
import com.lukhol.mathesis.android.performance.model.Test;
import com.lukhol.mathesis.android.performance.model.TestCode;
import com.lukhol.mathesis.android.performance.model.TestSummary;
import org.assertj.core.util.Lists;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@RunWith(SpringRunner.class)
public class TestRepositoryTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TestRepository testRepository;

    @Before
    public void setUp() {
        TestSummary testSummary = TestSummary.builder()
                .id("test-summary-id-1")
                .androidVersion("6.0.1")
                .build();

        Test test = Test.builder()
                .testCode(TestCode.ARITHMETIC)
                .applicationType(ApplicationType.CORDOVA)
                .time(12554)
                .testSummary(testSummary)
                .build();

        entityManager.persist(test);
        entityManager.persist(testSummary);

        entityManager.flush();
        entityManager.clear();
    }

    @org.junit.Test
    public void canLoadEntity() {
        List<String> testIds = testRepository.findAllTestSummaryIdsPresentedInTests();
        assertThat(testIds).hasSize(1);
    }

    @org.junit.Test
    public void canFindTestByTestSummaryId() {
        List<Test> tests = testRepository.findAllTestsByTestId("test-summary-id-1");
        assertThat(tests).hasSize(1);
    }

    @org.junit.Test
    public void canDeleteTestsByTestSummary() {
        String testSummaryId = "test-summary-id-1";
        List<Test> testListBeforeDelete = testRepository.findAllTestsByTestId(testSummaryId);
        assertThat(testListBeforeDelete).hasSize(1);

        testRepository.removeByTestSummaryIds(Lists.list(testSummaryId));
        entityManager.flush();

        List<Test> testListAfterDelete = testRepository.findAllTestsByTestId(testSummaryId);
        assertThat(testListAfterDelete).hasSize(0);
    }

    @After
    public void cleanUp() {
        entityManager.getEntityManager().createQuery("delete from Test").executeUpdate();
        entityManager.getEntityManager().createQuery("delete from TestSummary").executeUpdate();
    }
}