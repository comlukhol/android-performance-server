package com.lukhol.mathesis.android.performance.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lukhol.mathesis.android.performance.dto.StartupResultDto;
import com.lukhol.mathesis.android.performance.dto.TestSummaryMetadata;
import com.lukhol.mathesis.android.performance.mapper.TestMapper;
import com.lukhol.mathesis.android.performance.model.*;
import com.lukhol.mathesis.android.performance.repository.StartupTestRepository;
import com.lukhol.mathesis.android.performance.repository.TestRepository;
import com.lukhol.mathesis.android.performance.repository.TestSummaryRepository;
import com.lukhol.mathesis.android.performance.service.FixupService;
import com.lukhol.mathesis.android.performance.service.TestService;
import com.lukhol.mathesis.android.performance.service.TestServiceImpl;
import com.lukhol.mathesis.android.performance.validator.TestValidator;
import org.assertj.core.util.Lists;
import org.junit.Before;

import java.io.IOException;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestServiceImplTests {

    private TestService testService;
    private TestRepository testRepository = mock(TestRepository.class);
    private StartupTestRepository startupTestRepository = mock(StartupTestRepository.class);
    private TestSummaryRepository testSummaryRepository = mock(TestSummaryRepository.class);
    private FixupService fixupService = mock(FixupService.class);
    private TestMapper testMapper = new TestMapper();
    private TestValidator testValidator = new TestValidator();

    private String SUMMARY_ONE_ID = "test-summary-1";
    private String SUMMARY_TWO_ID = "test-summary-2";
    private String ANDROID_VERSION = "6.0.1, 7";

    private TestSummary testSummaryOne;
    private TestSummary testSummaryTwo;
    private List<String> listOfSummaryIdsToAverage = Lists.list(SUMMARY_ONE_ID, SUMMARY_TWO_ID);

    @Before
    public void setUp() throws IOException {
        TestServiceImplTestsDataProvider dataProvider = new TestServiceImplTestsDataProvider();
        ObjectMapper objectMapper = new ObjectMapper();

        testSummaryOne = objectMapper.readValue((java.lang.String)dataProvider.testSummaryOne(), TestSummary.class);
        testSummaryTwo = objectMapper.readValue((java.lang.String)dataProvider.testSummaryTwo(), TestSummary.class);

        testService = new TestServiceImpl(
                startupTestRepository,
                testRepository,
                testSummaryRepository,
                fixupService,
                testMapper,
                testValidator
        );
    }

    @org.junit.Test
    public void canAverageSummary() {
        when(testSummaryRepository.findAllByIds(listOfSummaryIdsToAverage))
                .thenReturn(Lists.list(testSummaryOne, testSummaryTwo));

        TestSummary averageTest = testService.getAverageForSummaryIds(listOfSummaryIdsToAverage);

        assertThat(averageTest.getAndroidVersion()).isEqualTo("6.0.1, 7");
        assertThat(averageTest.getId()).isEqualTo("test-summary-2, test-summary-1");

        for(ApplicationType appType : ApplicationType.values()) {
            assertThat(findTestResultByTestCodeAndApplicationType(averageTest, appType, TestCode.ARITHMETIC))
                    .isEqualTo(800);

            assertThat(findTestResultByTestCodeAndApplicationType(averageTest, appType, TestCode.JSON_SERIALIZATION))
                    .isEqualTo(1111);

            assertThat(findTestResultByTestCodeAndApplicationType(averageTest, appType, TestCode.JSON_DESERIALIZATION))
                    .isEqualTo(2746);
        }

        assertThat(averageTest.getStartupTest()).isNotNull();

        List<StartupResultDto> startupResultDtos = averageTest.getStartupTest().getStartupResults();
        for(StartupResultDto startupResultDto : startupResultDtos) {
            assertThat(startupResultDto.getThisTime()).isEqualTo(1000);
            assertThat(startupResultDto.getTotalTime()).isEqualTo(2000);
            assertThat(startupResultDto.getWaitTime()).isEqualTo(3000);
        }
    }

    @org.junit.Test
    public void canAverageSummaryForOneId() {
        List<String> oneIdList = Lists.list(SUMMARY_ONE_ID);
        when(testSummaryRepository.findAllByIds(oneIdList))
                .thenReturn(Lists.list(testSummaryOne));

        TestSummary averageTest = testService.getAverageForSummaryIds(oneIdList);

        assertThat(averageTest.getAndroidVersion()).isEqualTo("6.0.1");
        assertThat(averageTest.getId()).isEqualTo("test-summary-1");

        for(ApplicationType appType : ApplicationType.values()) {
            assertThat(findTestResultByTestCodeAndApplicationType(averageTest, appType, TestCode.ARITHMETIC))
                    .isEqualTo(1250);

            assertThat(findTestResultByTestCodeAndApplicationType(averageTest, appType, TestCode.JSON_SERIALIZATION))
                    .isEqualTo(990);

            assertThat(findTestResultByTestCodeAndApplicationType(averageTest, appType, TestCode.JSON_DESERIALIZATION))
                    .isEqualTo(1255);
        }

        assertThat(averageTest.getStartupTest()).isNotNull();

        List<StartupResultDto> startupResultDtos = averageTest.getStartupTest().getStartupResults();
        for(StartupResultDto startupResultDto : startupResultDtos) {
            assertThat(startupResultDto.getThisTime()).isEqualTo(1000);
            assertThat(startupResultDto.getTotalTime()).isEqualTo(2000);
            assertThat(startupResultDto.getWaitTime()).isEqualTo(3000);
        }
    }

    @org.junit.Test
    public void canGetAllTestSummariesMetadataOnlyOne() {
        TestSummaryMetadata testSummaryMetadataOne = TestSummaryMetadata.builder()
                .androidVersion("7")
                .id("test-summary-id-1")
                .deviceId("")
                .build();

        when(testSummaryRepository.findAllTestSummaryMetadata())
                .thenReturn(Lists.list(testSummaryMetadataOne));

        List<TestSummaryMetadata> summaryMetadataList = testService.getAllTestSummariesMetadata();
        assertThat(summaryMetadataList).hasSize(1);
        assertThat(summaryMetadataList.get(0)).matches(ts -> ts.getId().equals("test-summary-id-1"));
        assertThat(summaryMetadataList.get(0)).matches(ts -> ts.getDeviceId().equals(""));
        assertThat(summaryMetadataList.get(0)).matches(ts -> ts.getAndroidVersion().equals("7"));
    }

    @org.junit.Test
    public void canGetAllTestSummariesMetadataTwo() {
        TestSummaryMetadata testSummaryMetadataOne = TestSummaryMetadata.builder()
                .androidVersion("7")
                .id("test-summary-id-1")
                .deviceId("")
                .build();

        TestSummaryMetadata testSummaryMetadataTwo = TestSummaryMetadata.builder()
                .androidVersion("6.0.1")
                .id("test-summary-id-2")
                .deviceId("")
                .build();


        when(testSummaryRepository.findAllTestSummaryMetadata())
                .thenReturn(Lists.list(testSummaryMetadataOne, testSummaryMetadataTwo));

        List<TestSummaryMetadata> summaryMetadataList = testService.getAllTestSummariesMetadata();
        assertThat(summaryMetadataList).hasSize(2);

        Optional<TestSummaryMetadata> firstOptional = summaryMetadataList.stream()
                .filter(ts -> ts.getId().equals("test-summary-id-1"))
                .findFirst();

        assertThat(firstOptional).isPresent();
        assertThat(firstOptional.get()).matches(ts -> ts.getId().equals("test-summary-id-1"));
        assertThat(firstOptional.get()).matches(ts -> ts.getDeviceId().equals(""));
        assertThat(firstOptional.get()).matches(ts -> ts.getAndroidVersion().equals("7"));

        Optional<TestSummaryMetadata> secondOptional = summaryMetadataList.stream()
                .filter(ts -> ts.getId().equals("test-summary-id-2"))
                .findFirst();

        assertThat(secondOptional).isPresent();
        assertThat(secondOptional.get()).matches(ts -> ts.getId().equals("test-summary-id-2"));
        assertThat(secondOptional.get()).matches(ts -> ts.getDeviceId().equals(""));
        assertThat(secondOptional.get()).matches(ts -> ts.getAndroidVersion().equals("6.0.1"));
    }

    private long findTestResultByTestCodeAndApplicationType(TestSummary testSummary, ApplicationType applicationType, TestCode testCode) {
        for(Test test : testSummary.getTests()){
            if(applicationType.equals(test.getApplicationType()) && testCode.equals(test.getTestCode()))
                return test.getTime();
        }

        throw new IllegalStateException();
    }
}