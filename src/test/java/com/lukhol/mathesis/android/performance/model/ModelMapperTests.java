package com.lukhol.mathesis.android.performance.model;

import com.lukhol.mathesis.android.performance.ModelMapperConfig;
import com.lukhol.mathesis.android.performance.dto.StartupResultDto;
import com.lukhol.mathesis.android.performance.dto.StartupTestDto;
import com.lukhol.mathesis.android.performance.repository.TestSummaryRepository;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

@RunWith(SpringRunner.class)
public class ModelMapperTests {

    @Autowired
    private ModelMapper modelMapper;

    @org.springframework.boot.test.context.TestConfiguration
    static class TestConfiguration {
        @Bean
        ModelMapper modelMapper() {
            return new ModelMapperConfig().modelMapper();
        }
    }

    @org.junit.Test
    public void canMapStartupTestToDto() {
        List<StartupResultDto> startupResultDtos = new ArrayList<>();
        for(ApplicationType appType : ApplicationType.values()) {
            StartupResultDto startupResultDto = StartupResultDto.builder()
                    .applicationType(appType)
                    .thisTime(1000)
                    .totalTime(2000)
                    .waitTime(3000)
                    .build();

            startupResultDtos.add(startupResultDto);
        }

        StartupTestDto startupTestDto = StartupTestDto.builder()
                .testCode(TestCode.ARITHMETIC)
                .androidVersion("6.0.1")
                .summaryId("summary-id")
                .results(startupResultDtos)
                .build();

        StartupTest startupTest = modelMapper.map(startupTestDto, StartupTest.class);

        for(StartupResultDto startupResultDto : startupTest.getStartupResults()) {
            StartupResultDto expected = startupResultDtos.stream()
                    .filter(r -> r.getApplicationType() == startupResultDto.getApplicationType())
                    .findFirst()
                    .get();

            assertThat(startupResultDto.getTotalTime()).isEqualTo(expected.getTotalTime());
            assertThat(startupResultDto.getWaitTime()).isEqualTo(expected.getWaitTime());
            assertThat(startupResultDto.getThisTime()).isEqualTo(expected.getThisTime());
        }
    }
}