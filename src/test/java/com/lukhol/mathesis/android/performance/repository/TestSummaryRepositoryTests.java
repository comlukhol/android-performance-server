package com.lukhol.mathesis.android.performance.repository;

import com.lukhol.mathesis.android.performance.dto.TestSummaryMetadata;
import com.lukhol.mathesis.android.performance.model.TestSummary;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@RunWith(SpringRunner.class)
public class TestSummaryRepositoryTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TestSummaryRepository testSummaryRepository;

    @Before
    public void setUp() {
        TestSummary testSummaryOne = TestSummary.builder()
                .id("test-summary-id-1")
                .androidVersion("6.0.1")
                .build();

        TestSummary testSummaryTwo = TestSummary.builder()
                .id("test-summary-id-2")
                .androidVersion("7")
                .build();

        TestSummary testSummaryThree = TestSummary.builder()
                .id("test-summary-id-3")
                .build();

        TestSummary testSummaryFour = TestSummary.builder()
                .id("test-summary-id-4")
                .androidVersion("7")
                .build();

        entityManager.persist(testSummaryOne);
        entityManager.persist(testSummaryTwo);
        entityManager.persist(testSummaryThree);
        entityManager.persist(testSummaryFour);

        entityManager.flush();
        entityManager.clear();
    }

    @org.junit.Test
    public void canGetByIds() {
        List<TestSummary> testSummaries = testSummaryRepository.findAllByIds(
                Lists.list("test-summary-id-1", "test-summary-id-3", "dsadsa")
        );

        assertThat(testSummaries).hasSize(2);
    }

    @org.junit.Test
    public void canGetAllTestSummaryIds() {
        List<String> allTestSummaryIds = testSummaryRepository.findAllIds();
        assertThat(allTestSummaryIds).hasSize(4);
    }

    @org.junit.Test
    public void canDeleteByIds() {
        testSummaryRepository.deleteByIds(
                Lists.list("test-summary-id-1", "test-summary-id-3")
        );

        entityManager.flush();
        entityManager.clear();

        List<String> allTestSummaryIds = testSummaryRepository.findAllIds();
        assertThat(allTestSummaryIds).hasSize(2);
        assertThat(allTestSummaryIds).contains("test-summary-id-2", "test-summary-id-4");
    }

    @org.junit.Test
    public void canGetAllTestSummariesByAndroidVersion() {
        List<String> testSummaries = testSummaryRepository.findAllIdsByAndroidVersion("7");
        assertThat(testSummaries).hasSize(2);
        assertThat(testSummaries)
                .contains("test-summary-id-2", "test-summary-id-4")
                .doesNotContain("test-summary-id-1","test-summary-id-3");
    }

    @org.junit.Test
    public void canGetAllAndroidVersions() {
        List<String> allAndroidVersions = testSummaryRepository.findAllAndroidVersions();
        assertThat(allAndroidVersions).hasSize(2);
        assertThat(allAndroidVersions).contains("7", "6.0.1");
        assertThat(allAndroidVersions).startsWith("6.0.1");
    }

    @org.junit.Test
    public void canGetAllTestSummaryMetadata() {
        entityManager.getEntityManager().createQuery("delete from TestSummary").executeUpdate();
        entityManager.flush();
        entityManager.clear();

        TestSummary testSummaryOne = TestSummary.builder()
                .id("test-summary-id-1")
                .androidVersion("6.0.1")
                .build();

        entityManager.persist(testSummaryOne);
        entityManager.flush();
        entityManager.clear();

        List<TestSummaryMetadata> testSummaryMetadataList = testSummaryRepository.findAllTestSummaryMetadata();
        assertThat(testSummaryMetadataList).hasSize(1);
        assertThat(testSummaryMetadataList.get(0)).matches(ts -> ts.getAndroidVersion().equals("6.0.1"));
        assertThat(testSummaryMetadataList.get(0)).matches(ts -> ts.getId().equals("test-summary-id-1"));
        assertThat(testSummaryMetadataList.get(0)).matches(ts -> ts.getDeviceId().equals(""));
    }
}