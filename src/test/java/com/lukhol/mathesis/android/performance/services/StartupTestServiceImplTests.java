package com.lukhol.mathesis.android.performance.services;

import com.lukhol.mathesis.android.performance.ModelMapperConfig;
import com.lukhol.mathesis.android.performance.dto.StartupResultDto;
import com.lukhol.mathesis.android.performance.dto.StartupTestDto;
import com.lukhol.mathesis.android.performance.model.ApplicationType;
import com.lukhol.mathesis.android.performance.model.StartupTest;
import com.lukhol.mathesis.android.performance.model.TestCode;
import com.lukhol.mathesis.android.performance.model.TestSummary;
import com.lukhol.mathesis.android.performance.repository.StartupTestRepository;
import com.lukhol.mathesis.android.performance.repository.TestSummaryRepository;
import com.lukhol.mathesis.android.performance.service.StartupTestService;
import com.lukhol.mathesis.android.performance.service.StartupTestServiceImpl;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class StartupTestServiceImplTests {

    private StartupTestService startupTestService;
    private StartupTestRepository startupTestRepositoryMock;
    private TestSummaryRepository testSummaryRepositoryMock;

    @Autowired
    private ModelMapper modelMapper;

    @org.springframework.boot.test.context.TestConfiguration
    static class TestConfiguration {
        @Bean
        ModelMapper modelMapper() {
            return new ModelMapperConfig().modelMapper();
        }
    }

    private List<StartupResultDto> startupResultDtos;
    private StartupTestDto startupTestDto;

    @Before
    public void setUp() {
        startupResultDtos = new ArrayList<>();
        for (ApplicationType appType : ApplicationType.values()) {
            StartupResultDto startupResultDto = StartupResultDto.builder()
                    .applicationType(appType)
                    .thisTime(1000)
                    .totalTime(2000)
                    .waitTime(3000)
                    .build();

            startupResultDtos.add(startupResultDto);
        }

        startupTestDto = StartupTestDto.builder()
                .testCode(TestCode.ARITHMETIC)
                .androidVersion("6.0.1")
                .summaryId("summary-id")
                .results(startupResultDtos)
                .build();

        startupTestRepositoryMock = prepareStartupTestRepositoryMock();
        testSummaryRepositoryMock = prepareTestSummaryRepositoryMock();
        startupTestService = new StartupTestServiceImpl(startupTestRepositoryMock, testSummaryRepositoryMock, modelMapper);
    }

    private TestSummaryRepository prepareTestSummaryRepositoryMock() {
        TestSummaryRepository mock = mock(TestSummaryRepository.class);
        when(mock.findById(startupTestDto.getSummaryId()))
                .thenReturn(Optional.empty());
        return mock;
    }

    private StartupTestRepository prepareStartupTestRepositoryMock() {
        StartupTestRepository mock = mock(StartupTestRepository.class);

        doAnswer(answer -> {
            System.out.println("After execution of StartupTestRepository::save(StartupTest.class)");
            return null;
        }).when(mock).save(any(StartupTest.class));

        //doThrow(new Exception()).when(mock).findByTestSummaryId(any(String.class));

        return mock;
    }

    @org.junit.Test
    public void saveTest() {
        ArgumentCaptor<StartupTest> startupTestArgument = ArgumentCaptor.forClass(StartupTest.class);

        startupTestService.save(startupTestDto);

        //verify(testSummaryRepositoryMock, times(1)).findById(startupTestDto.getSummaryId());
        verify(testSummaryRepositoryMock, times(1)).save(any(TestSummary.class));
        verify(startupTestRepositoryMock, times(1)).save(startupTestArgument.capture());

        assertThat(startupTestArgument.getValue().getStartupResults()).hasSize(3);

        int i = 0;
        for (ApplicationType appType : ApplicationType.values()) {
            StartupResultDto actual = startupTestArgument.getValue().getStartupResults().get(i++);
            StartupResultDto expected = startupResultDtos.stream().filter(a -> a.getApplicationType() == appType).findFirst().get();

            //AssertJ
            assertThat(actual.getApplicationType()).isEqualByComparingTo(expected.getApplicationType());
            assertThat(actual.getThisTime()).isEqualTo(expected.getThisTime());
            assertThat(actual.getWaitTime()).isEqualTo(expected.getWaitTime());
            assertThat(actual.getTotalTime()).isEqualTo(expected.getTotalTime());

            //JUnit
            assertEquals(actual.getApplicationType(), expected.getApplicationType());
            assertEquals(actual.getThisTime(), expected.getThisTime());
            assertEquals(actual.getWaitTime(), expected.getWaitTime());
            assertEquals(actual.getTotalTime(), expected.getTotalTime());
        }
    }
}