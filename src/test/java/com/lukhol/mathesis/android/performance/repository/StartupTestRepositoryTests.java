package com.lukhol.mathesis.android.performance.repository;

import com.lukhol.mathesis.android.performance.dto.StartupResultDto;
import com.lukhol.mathesis.android.performance.model.*;
import org.assertj.core.util.Lists;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@RunWith(SpringRunner.class)
public class StartupTestRepositoryTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private StartupTestRepository startupTestRepository;

    private String testSummaryId = "test-summary-id";

    private Test test;
    private StartupTest startupTest;
    private TestSummary testSummary;
    private List<StartupResultDto> startupResultDtos;


    @Before
    public void setUp() {
        startupResultDtos = new ArrayList<>();
        for (ApplicationType appType : ApplicationType.values()) {
            StartupResultDto startupResultDto = StartupResultDto.builder()
                    .applicationType(appType)
                    .thisTime(1000)
                    .totalTime(2000)
                    .waitTime(3000)
                    .build();

            startupResultDtos.add(startupResultDto);
        }

        testSummary = TestSummary.builder()
                .id(testSummaryId)
                .build();

        test = Test.builder()
                .testCode(TestCode.ARITHMETIC)
                .applicationType(ApplicationType.CORDOVA)
                .time(12554)
                .testSummary(testSummary)
                .build();

        startupTest = StartupTest.builder()
                .startupResults(startupResultDtos)
                .testSummary(testSummary)
                .build();

        testSummary.setStartupTest(startupTest);

        entityManager.persist(testSummary);
        entityManager.persist(test);
        entityManager.persist(startupTest);

        entityManager.flush();
        entityManager.clear();
    }

    @org.junit.Test
    public void canFindByTestSummaryId() {
        Optional<StartupTest> startupTestOptional = startupTestRepository.findByTestSummaryId(testSummaryId);
        assertThat(startupTestOptional).isPresent();
    }

    @org.junit.Test
    public void cannotFind_notFoundNothing() {
        cleanUp();
        entityManager.flush();
        entityManager.clear();

        Optional<StartupTest> startupTestOptional = startupTestRepository.findByTestSummaryId(testSummaryId);
        assertThat(startupTestOptional).isNotPresent();
    }

    @org.junit.Test
    public void canDeleteBySummaryIds() {
        Optional<StartupTest> startupTestOptional = startupTestRepository.findByTestSummaryId(testSummaryId);
        assertThat(startupTestOptional).isPresent();

        startupTestRepository.deleteBySummaryIds(Lists.list(testSummaryId));
        entityManager.flush();
        entityManager.clear();

        Optional<StartupTest> startupTestOptionalAfterDelete = startupTestRepository.findByTestSummaryId(testSummaryId);
        assertThat(startupTestOptionalAfterDelete).isNotPresent();
    }

    @org.junit.Test
    public void canDeleteBySummaryId() {
        Optional<StartupTest> startupTestOptional = startupTestRepository.findByTestSummaryId(testSummaryId);
        assertThat(startupTestOptional).isPresent();

        startupTestRepository.deleteBySummaryId(testSummaryId);
        entityManager.flush();
        entityManager.clear();

        Optional<StartupTest> startupTestOptionalAfterDelete = startupTestRepository.findByTestSummaryId(testSummaryId);
        assertThat(startupTestOptionalAfterDelete).isNotPresent();
    }

    @org.junit.Test
    public void canDeleteChildrenOfStartupTestEmbeddedBud() {
        Optional<StartupTest> startupTestOptional = startupTestRepository.findByTestSummaryId(testSummaryId);
        assertThat(startupTestOptional).isPresent();
        assertThat(startupTestOptional.get().getStartupResults()).hasSize(3);

        startupTestRepository.deleteChildrenOfStartupTestEmbeddedBug(Lists.list(testSummaryId));
        entityManager.flush();
        entityManager.clear();

        Optional<StartupTest> startupTestOptionalAfterTestDeleted = startupTestRepository.findByTestSummaryId(testSummaryId);
        assertThat(startupTestOptionalAfterTestDeleted).isPresent();
        assertThat(startupTestOptionalAfterTestDeleted.get().getStartupResults()).hasSize(0);
    }

    @After
    public void cleanUp() {
        entityManager.getEntityManager().createQuery("delete from Test").executeUpdate();
        entityManager.getEntityManager().createQuery("delete from StartupTest").executeUpdate();
        entityManager.getEntityManager().createQuery("delete from TestSummary").executeUpdate();
        entityManager.flush();
        entityManager.clear();
    }
}